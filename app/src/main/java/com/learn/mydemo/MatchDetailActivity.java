package com.learn.mydemo;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class MatchDetailActivity extends AppCompatActivity {
    ProgressDialog pd;
    private WebView webview1;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_detail);

        adView();
        initialize();
        initializeLogic();
    }

    private void adView() {
        mAdView = (AdView) findViewById(R.id.adView);
//        mAdView.setAdSize(AdSize.BANNER);
//        mAdView.setAdUnitId(getString(R.string.banner_home_footer));

        AdRequest adRequest = new AdRequest.Builder()
//                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                // Check the LogCat to get your test device ID
//                .addTestDevice("EF9DAEF6F4C86A7A3F8815F4A00FFC39")
                .build();

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdClosed() {
                Toast.makeText(getApplicationContext(), "Ad is closed!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Toast.makeText(getApplicationContext(), "Ad failed to load! error code: " + errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                Toast.makeText(getApplicationContext(), "Ad left application!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }
        });

        mAdView.loadAd(adRequest);
    }

    private void initialize() {
        pd = new ProgressDialog(MatchDetailActivity.this);
        pd.setMessage("Please wait ...");
        pd.setCancelable(false);
        webview1 = (WebView) findViewById(R.id.webview1);
        webview1.getSettings().setJavaScriptEnabled(true);
        webview1.getSettings().setSupportZoom(true);
        webview1.setWebViewClient(new WebViewClient() {
            public void onPageStarted(WebView _view, String _url, Bitmap _favicon) {
                pd.show();

                super.onPageStarted(_view, _url, _favicon);
            }

            public void onPageFinished(WebView _view, String _url) {
                pd.dismiss();
                super.onPageFinished(_view, _url);
            }
        });
    }

    private void initializeLogic() {
        getSupportActionBar().setTitle("Dream 11 Predictions");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#C61D23")));
        Bundle b = getIntent().getExtras();
        if (b.getString("matchno").equals("1")) {
            webview1.loadUrl("https://dream11match1.blogspot.com");
        } else if (b.getString("matchno").equals("2")) {
            webview1.loadUrl("https://dream11match2.blogspot.com");
        } else if (b.getString("matchno").equals("3")) {
            webview1.loadUrl("https://dream11match3.blogspot.com");
        } else if (b.getString("matchno").equals("4")) {
            webview1.loadUrl("https://dream11match4.blogspot.com");
        }

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}