package com.learn.mydemo;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    LinearLayout lltodaymatch, llshare, llrate;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adView();
        initall();
    }

    private void adView() {
        mAdView = (AdView) findViewById(R.id.adView);
//        mAdView.setAdSize(AdSize.BANNER);
//        mAdView.setAdUnitId(getString(R.string.banner_home_footer));

        AdRequest adRequest = new AdRequest.Builder()
//                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                // Check the LogCat to get your test device ID
//                .addTestDevice("EF9DAEF6F4C86A7A3F8815F4A00FFC39")
                .build();

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdClosed() {
                Toast.makeText(getApplicationContext(), "Ad is closed!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Toast.makeText(getApplicationContext(), "Ad failed to load! error code: " + errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                Toast.makeText(getApplicationContext(), "Ad left application!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }
        });

        mAdView.loadAd(adRequest);
    }

    public void initall() {
        llshare = (LinearLayout) findViewById(R.id.sherell);
        llrate = (LinearLayout) findViewById(R.id.llrate);
        llrate.setOnClickListener(this);
        llshare.setOnClickListener(this);
        lltodaymatch = (LinearLayout) findViewById(R.id.lltodaymatch);
        lltodaymatch.setOnClickListener(this);

    }


    public void gotoother(View view) {

        if (ConnectivityDetector.isConnectingToInternet(this)) {

            Intent i = new Intent(getApplicationContext(), HowDoIPlayActivity.class);
            i.putExtra("type", "" + view.getTag());
            startActivity(i);
        } else {
            AlertDialogUtility.showInternetAlert(this);
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.llrate) {
            String appName = getPackageName();
            try {
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + appName)));
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://play.google.com/store/apps/details?id=" + appName)));
            }
        } else if (view.getId() == R.id.sherell) {
            String appName = getPackageName();
            String shareBody = "Hey Download the Dream 11 prediction application";
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share Using"));

        } else if (view.getId() == R.id.lltodaymatch) {

            if (ConnectivityDetector.isConnectingToInternet(this)) {


                Intent i = new Intent(getApplicationContext(), MatchesActivity.class);
                startActivity(i);
            } else {
                AlertDialogUtility.showInternetAlert(this);
            }
        }
    }
}
