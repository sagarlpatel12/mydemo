package com.learn.mydemo;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

public class AlertDialogUtility {

    public static final String STR_INTERNET_ALERT_TITLE = "Internet Connection Required";
    public static final String STR_INETRNET_ALERT_MESSAGE = "Please check your internet connection and try again.";

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void showAlert(Context context, String msg) {
        new AlertDialog.Builder(context).setIcon(0)
                .setTitle(context.getString(R.string.app_name)).setMessage(msg)
                .setCancelable(false).setNeutralButton("OK", null).show();
    }

    public static void showInternetAlert(Context context) {
        new AlertDialog.Builder(context).setIcon(0)
                .setTitle(STR_INTERNET_ALERT_TITLE)
                .setMessage(STR_INETRNET_ALERT_MESSAGE)
                .setCancelable(false).setNeutralButton("OK", null).show();
    }

    public static void CustomAlert(Context context, String title,
                                   String message, String Positive_text, String Negative_text,
                                   DialogInterface.OnClickListener PositiveListener,
                                   DialogInterface.OnClickListener NegativeListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle(title).setMessage(message)
                .setPositiveButton(Positive_text, PositiveListener)
                .setNegativeButton(Negative_text, NegativeListener);
        AlertDialog dialog = builder.create();
        dialog.show();
    }



}